package Base;

/**
 * QuickFind - Algoritmo básico de coste cuadratico.
 * @author Josep Dols
 */
public class QuickFindUF {
    private int[] id;
    
    /**
     * Constructor por defecto para QuickFindUF.
     * @param N Número de elementos de la estructura.
     */
    public QuickFindUF(int N){
        id = new int[N];
        for(int i=0; i<N; i++){
            id[i] = i;
        }
    }
    
    /**
     * Método para saber si dos nodos están conectados.
     * @param p Primer nodo.
     * @param q Segundo nodo.
     * @return true - Si estan conectados. false - Si no estan conectados.
     */
    public boolean connected(int p, int q){
        return id[p]==id[q];
    }
    /**
     * Método para unir dos nodos separados.
     * @param p Primer nodo.
     * @param q Segundo nodo.
     */
    public void union(int p, int q){
        int pid = id[p];
        int qid = id[q];
        for(int i=0; i<id.length; i++){
            if(id[i]==pid){
                id[i] = qid;
            }
        }
    }
}
