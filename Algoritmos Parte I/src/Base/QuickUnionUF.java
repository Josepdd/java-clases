package Base;

/**
 * QuickUnion - Algoritmo básico de coste.
 * @author Josep Dols
 */
public class QuickUnionUF {
    private int[] id;
    
    /**
     * Constructor por defecto para QuickUnion.
     * @param N Número de elementos de la estructura.
     */
    public QuickUnionUF(int N){
        id = new int[N];
        for(int i=0; i<N; i++){
            id[i] = i;
        }
    }
    
    /**
     * Método para obtener la raiz de un elemento.
     * @param i Elemento del que obtener la raiz.
     * @return Raiz del elemento.
     */
    private int root(int i){
        while(i!=id[i]){
            id[i] = id[id[i]];
            i = id[i];
        }
        return i;
    }
    /**
     * Método para saber si dos elementos estan conectados.
     * @param p Primer elemento.
     * @param q Segundo elemento.
     * @return true - Si estan conectados. false - Si no estan conectados.
     */
    public boolean connected(int p, int q){
        return root(p)==root(q);
    }
    /**
     * Método para conectar dos elementos.
     * @param p Primer elemento.
     * @param q Segundo elemento.
     */
    public void union(int p, int q){
        int i = root(p);
        int j = root(q);
        id[i] = j;
    }
}
