package Nucleo;


/**
 * Clase para inicializar la ventana y las variables necesarias para realizar la conversion.
 * @author Josep Dols
 */
public class Base {
    public static void main(String[] args){
        VentanaPrincipal a  = new VentanaPrincipal();
        a.setVisible(true);
    }
    
    public static Hora calcular(Hora h1, int z1, int z2){
        z1 += 12;
        z2 += 12;
        int dif = (int)Math.abs(z2-z1);
        if(z2>z1){
           Hora h2 = new Hora(h1.getHora()+dif,h1.getMin());
           h2.arreglar();
           return h2;
        }else if(z2<z1){
            Hora h3 = new Hora(h1.getHora()-dif,h1.getMin());
            h3.arreglar();
            return h3;
        }else{
            return h1;
        }
    }
}
