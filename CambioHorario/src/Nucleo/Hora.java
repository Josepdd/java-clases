package Nucleo;

/**
 * Clase Hora para llevar el control de las horas del conversor.
 * @author Josep Dols
 */
public class Hora {
    private int horas, minutos;
    
    /**
     *  Hora correspondiente a las hor horas y min minutos.
     *  Precondicion: 0<=hor<24, 0<=min<60.
     * @param h Horas del objeto.
     * @param m Minutos del objeto.
     */
    public Hora(int h, int m){
        this.horas = h;
        this.minutos = m;
    }
    
    /**
     * Hora (horas y minutos) actual UTC (tiempo coordinado universal).
     */
    public Hora(){
        int tRestante = (int) (System.currentTimeMillis() % (24*3600*1000));
        int segHoy = (int)tRestante/1000;
        int minHoy = (int)segHoy/60;
        int horaHoy = minHoy/60;
        minHoy = minHoy%60;
        this.horas = horaHoy;
        this.minutos = minHoy;
    }
    
    /** Consulta las horas de la Hora 
     * @return h Hora del objeto.
     */ 
    public int getHora(){
        return this.horas;
    }
    /** Consulta los minutos de la Hora 
     * @return m Minutos del objeto
     */ 
    public int getMin(){
        return this.minutos;
    }   
    /** Modifica las horas de la Hora 
     * @param hor Nueva hora del objeto
     */ 
    public void setHora(int hor){
        this.horas = hor;
    }   
    /** Modifica los minutos de la Hora 
     * @param min Nuevos minutos del objeto
     */ 
    public void setMin(int min){
        this.minutos = min;
    }
    
    /**
     * Metodo para corregir posibles errores en el formato del objeto.
     */
    public void arreglar(){
        if(this.horas>23){
            this.horas = this.horas - 24;
            this.arreglar();
        }
        if(this.minutos>59){
            this.minutos = this.minutos - 60;
            this.horas++;
            this.arreglar();
        }
    }
}
