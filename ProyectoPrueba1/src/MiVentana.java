/**
 *
 * @author Josep
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class MiVentana extends JFrame{
    public MiVentana(){
        super("Ventana de prueba");
        setSize(400,300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        Container cp = getContentPane();
        cp.setLayout(new FlowLayout());
        
        JLabel l1 = new JLabel("Nombre: ");
        JTextField tf1 = new JTextField(20);
        JButton b1 = new JButton("Cerrar");
        JButton b2 = new JButton("Abrir");
                
        b1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);  
            }
        });
        
        b2.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser jfc1 = new JFileChooser();
                int returnVal = jfc1.showOpenDialog(jfc1);
                if(returnVal == JFileChooser.APPROVE_OPTION){
                    File file = jfc1.getSelectedFile();
                    try{
                        Desktop.getDesktop().open(file);
                    }catch(IOException e1){
                        System.out.println("Problema accediendo a los archivos");
                    }
                }else{
                    System.out.println("Acceso cancelado");
                }
            }
        });
        
        cp.add(l1);
        cp.add(tf1);
        cp.add(b1);
        cp.add(b2);
    }
}
