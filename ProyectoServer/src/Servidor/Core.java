package Servidor;

/**
 *
 * @author Josep Dols
 */
public class Core {
    
    protected int temp, humid;
    protected boolean waterStat, airStat;
    
    private int desTemp, desHumid;
    private boolean desWaterStat, desAirStat;
    
    public Core(){
        this.airStat = false;
        this.waterStat = false;
        this.temp = 25;
        this.humid = 50;
    }
    
    public Core(int temperatura, int humedad, boolean agua, boolean ventanas){
        this.airStat = ventanas;
        this.waterStat = agua;
        this.temp = temperatura;
        this.humid = humedad;
    }
    
    public boolean[] sendTemp(String inputTemp){
        int actualTemp = Integer.parseInt(inputTemp);
        reactTemp(actualTemp);
        boolean[] aux = {this.airStat,this.waterStat};
        return aux;
    }
    
    public void reactTemp(int actTemp){
        if(actTemp>desTemp){
            this.airStat = true;
        }else if(actTemp<desTemp){
            this.airStat = false;
        }
    }
}
