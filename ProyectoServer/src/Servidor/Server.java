package Servidor;

import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Josep Dols
 */
public class Server {
    protected ServerSocket server;
    protected Socket clientSocket;
    protected BufferedReader entrada;
    protected PrintWriter salida;
    private Core nucleo = new Core();
    
    public void openSocket(int puerto){
        try{
            server = new ServerSocket(puerto); 
        }catch (IOException e) {
            System.out.println("Could not listen on port: " + puerto);
            System.exit(-1);
        }
    }
    
    public void closeSocket(){
        try{
        entrada.close();
        salida.close();
        server.close();
        clientSocket.close();
        }catch(IOException e3){
            System.err.println("Closed streams");
        }
    }
    
    public void clientInput(){
        try{
            clientSocket = server.accept();
        }catch (IOException e) {
            System.err.println("Accept failed.");
            System.exit(1);
        }
        
        try{
        entrada = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        salida = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
        }catch(IOException e2){
            System.out.println("Error writing/reading ");
            System.exit(-1);
        }
    }
    
    public String waitInput(){
        String instruct;
        while(true){
            try{
                instruct = entrada.readLine();
                if(instruct==null){
                    wait(5);
                }else{
                    return instruct;
                }
            }catch(IOException e){
                //nothing
            } catch (InterruptedException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void analyzeInput(String instruction){
        String separadores = "[\\p{Space}\\p{Punct}\\p{Digit}!?]+";
        String aux[] = instruction.split(separadores);
        switch(aux[1]){
            case "TEMP":
                boolean[] react = nucleo.sendTemp(aux[2]);
                salida.print(react[0] + "." + react[1]);
                break;
            case "HUMID":
        }
    }
}   