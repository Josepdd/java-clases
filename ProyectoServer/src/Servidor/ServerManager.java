package Servidor;

import java.net.*;
import java.io.*;
import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author Josep Dols
 */
public class ServerManager {
    Scanner teclado = new Scanner(System.in).useLocale(Locale.US);
    String stat = "default";
    Server servidor = new Server();
    public static void main(String[] args){
        //Server servidor = new Server();
        
    }
    
    public int opciones(Scanner tec){
        System.out.println("Status: " + stat);
        System.out.println("Opciones para el servidor: ");
        System.out.println("1. Iniciar servidor.");
        System.out.println("2. Esperar entradas.");
        System.out.println("3. Cerrar servidor");
        int opc = tec.nextInt();
        return opc;
    }
    
    public void selector(){
        int opc = opciones(teclado);
        switch(opc){
            case 1:
                System.out.println("Introduce el puerto para el servidor: ");
                int port = teclado.nextInt();
                servidor.openSocket(port);
                stat = "Servidor abierto.";
                break;
            case 2:
                String ins = servidor.waitInput();
                servidor.analyzeInput(ins);
        }
    }
}
